﻿using UnityEditor;
using UnityEngine;
using UnityEngine.VFX;

namespace Localization
{
    [System.Serializable]
    public class LocalizationLanguageVocabularyPair
    {
        [SerializeField] private SystemLanguage language;
        public SystemLanguage Language => language;

        [SerializeField] private LocalizationVocabulary vocabulary;
        public LocalizationVocabulary Vocabulary => vocabulary;
    }
}