﻿using System;
using TMPro;

namespace Utils
{
    public static class StringExtensions
    {
        public static float ParseFloat(this string text, float defaultValue = 0)
        {
            try
            {
                if (!string.IsNullOrEmpty(text))
                {
                    text = text.Replace('.', ',');
                    
                    float result = (float) Convert.ToDouble(text);

                    return result;
                }

                return defaultValue;
            }
            catch (Exception e)
            {
                return defaultValue;
            }   
        }
    }
}