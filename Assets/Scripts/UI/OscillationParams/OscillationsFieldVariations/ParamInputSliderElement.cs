﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.OscillationParams
{
    public class ParamInputSliderElement : BaseParamInputElement
    {
        [SerializeField] private Slider slider;
        [SerializeField] private TMP_Text textValue;
        
        protected override void Awake()
        {
            base.Awake();
            slider.onValueChanged.AddListener(OnInputUpdate);

            slider.minValue = minValue;
            slider.maxValue = maxValue;
        }

        public override void SetShowingValue(float value)
        {
            base.SetShowingValue(value);
            textValue.text = value.ToString("0.00");
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            slider.onValueChanged.RemoveListener(OnInputUpdate);
        }
    }
}