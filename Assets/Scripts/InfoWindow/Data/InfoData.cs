﻿using System;
using UnityEngine;

namespace Info.Data
{
    [Serializable]
    public class InfoData
    {
        public string KeyText;
        public Sprite FormulaSprite;
    }
}