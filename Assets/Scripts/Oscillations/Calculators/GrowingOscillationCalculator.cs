﻿using Oscillations.Base.Data;
using TMPro;
using UI.OscillationParams;
using UnityEngine;
using Utils;

namespace Oscillations.Base.Calculators
{
    public class GrowingOscillationCalculator : MonoBehaviour, ICalculator
    {
        [Header("Values")]
        [SerializeField] private OscillationGrowingData data;

        public OscillationData GetData() => data;

        private OscillationsParamsView _paramsView;

        public float Calculate(float time)
        {
            return data.Amplitude * (Mathf.Pow(2.718f, data.GrowingValue * time))
                                  * Mathf.Sin(data.W * time + data.Phase);
        }
        
        private void OnEnable()
        {
            _paramsView = OscillationParamsWindow.Instance.ShowParams(data.Type);

            foreach (var field in _paramsView.Fields)
            {
                Subscribe(field.FieldType, field.InputElement);
            }
        }

        private void Subscribe(OscillationFieldType type, BaseParamInputElement element)
        {
            switch (type)
            {
                case OscillationFieldType.Amplitude:
                    element.OnValueChanged += AmplitudeUpdate;
                    element.SetShowingValue(data.Amplitude);
                    break;
                
                case OscillationFieldType.Frequency:
                    element.OnValueChanged += FrequencyUpdate;
                    element.SetShowingValue(data.Frequency);
                    break;
                
                case OscillationFieldType.Phase:
                    element.OnValueChanged += PhaseUpdate;
                    element.SetShowingValue(data.Phase);
                    break;

                case OscillationFieldType.Growing:
                    element.OnValueChanged += GrowingUpdate;
                    element.SetShowingValue(data.GrowingValue);
                    break;
            }
        }
        
        private void Unsubscribe(OscillationFieldType type, BaseParamInputElement element)
        {
            switch (type)
            {
                case OscillationFieldType.Amplitude:
                    element.OnValueChanged -= AmplitudeUpdate;
                    break;
                
                case OscillationFieldType.Frequency:
                    element.OnValueChanged -= FrequencyUpdate;
                    break;
                
                case OscillationFieldType.Phase:
                    element.OnValueChanged -= PhaseUpdate;
                    break;

                case OscillationFieldType.Growing:
                    element.OnValueChanged -= GrowingUpdate;
                    break;
            }
        }
        
        #region InputFields UpdateEventMethods
        private void AmplitudeUpdate(float value)
        {
            data.Amplitude = value;
        }
        
        private void FrequencyUpdate(float value)
        {
            data.Frequency = value;
        }
        
        private void PhaseUpdate(float value)
        {
            data.Phase = value;
        }
        
        private void GrowingUpdate(float value)
        {
            data.GrowingValue = value;
        }
        #endregion
        
        private void OnDisable()
        {
            foreach (var field in _paramsView.Fields)
            {
                Unsubscribe(field.FieldType, field.InputElement);
            }
        }
    }
}