﻿using System.Collections.Generic;
using Oscillations.Base.Data;
using UnityEngine;
using Utils;

namespace UI.OscillationParams
{
    public class OscillationParamsWindow : MonoBehaviour
    {
        public static OscillationParamsWindow Instance;
        public OscillationParamsWindow()
        {
            Instance = this;
        }
        
        [SerializeField] private GameObject paramsContainer;
        [SerializeField] private OscillationsParamsView paramsViewPrefab;

        private List<OscillationsParamsView> pullObjects = new List<OscillationsParamsView>();

        private void HideAll()
        {
            foreach (var view in pullObjects)
            {
                view.SetActive(false);
            }
        }

        private OscillationsParamsView GetFreeParamsView()
        {
            if (pullObjects.Count == 0)
            {
                pullObjects.Add(Instantiate(paramsViewPrefab, paramsContainer.transform));
                return pullObjects[0];
            }

            foreach (var view in pullObjects)
            {
                if (!view.IsActive)
                    return view;
            }
            
            pullObjects.Add(Instantiate(paramsViewPrefab, paramsContainer.transform));
            return pullObjects[pullObjects.Count - 1];
        }
        
        public OscillationsParamsView ShowParams(OscillationType type)
        {
            HideAll();

            var view = GetFreeParamsView();
            ConfigurateParamsView(view, type);

            view.SetActive(true);
            
            return view;
        }
        
        public OscillationsParamsView[] ShowParams(OscillationType[] types)
        {
            HideAll();

            OscillationsParamsView[] views = new OscillationsParamsView[types.Length];
            
            for (var i = 0; i < types.Length; i++)
            {
                var view = GetFreeParamsView();
                ConfigurateParamsView(view, types[i]);

                view.SetActive(true);

                views[i] = view;
            }

            return views;
        }

        private void ConfigurateParamsView(OscillationsParamsView targetView, OscillationType type)
        {
            switch (type)
            {
                case OscillationType.Harmonic:
                    targetView.Configurate(type, new OscillationFieldType[]
                    {
                        OscillationFieldType.Amplitude,
                        OscillationFieldType.Frequency,
                        OscillationFieldType.Phase,
                    });
                    break;
                
                
                case OscillationType.Grow:
                    targetView.Configurate(type, new OscillationFieldType[]
                    {
                        OscillationFieldType.Amplitude,
                        OscillationFieldType.Frequency,
                        OscillationFieldType.Phase,
                        OscillationFieldType.Growing
                    });
                    break;
                
                
                case OscillationType.Damped:
                    targetView.Configurate(type, new OscillationFieldType[]
                    {
                        OscillationFieldType.Amplitude,
                        OscillationFieldType.Frequency,
                        OscillationFieldType.Phase,
                        OscillationFieldType.Damping
                    });
                    break;
            }
        }
    }
}