﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Oscillations.Base.Functions
{
    public class FunctionsManager : MonoBehaviour
    {
        public static FunctionsManager Instance;
        public FunctionsManager()
        {
            if (Instance == null)
                Instance = this;
        }
        
        [SerializeField] private GameObject[] calculatorPrefabs;
        [SerializeField] private Transform container;
        
        private readonly Dictionary<Type, GameObject> _functions = new Dictionary<Type, GameObject>();
        
        public GameObject GetFunction(Type type) 
        {
            if (!_functions.ContainsKey(type))
            {
                _functions.Add(type, CreateNewFunction(type));
            }

            return _functions[type];
        }

        private GameObject CreateNewFunction(Type type)
        {
            foreach (var calculatorPrefab in calculatorPrefabs)
            {
                IFunction calculator = calculatorPrefab.GetComponent<IFunction>();
                if (calculator.GetType() == type)
                {
                    return Instantiate(calculatorPrefab, container);
                }
            }

            return null;
        }
    }
}