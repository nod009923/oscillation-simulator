﻿namespace Oscillations.Base
{
    public interface IFunction
    {
        float Calculate(float time);

        float Period();
    }
}