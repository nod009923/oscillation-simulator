﻿using System;
using TMPro;
using UnityEngine;

namespace Localization
{
    public class LocalizationText : MonoBehaviour
    {
        [SerializeField] private TMP_Text target;
        [SerializeField] private string key;

        private void Start()
        {
            target.text = LocalizationManager.Instantiate.Translate(key);
        }
    }
}