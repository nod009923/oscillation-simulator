﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Info
{
    public class InfoWindow : MonoBehaviour
    {
        public static InfoWindow Instance;
        public InfoWindow()
        {
            if (Instance == null)
                Instance = this;
        }

        [SerializeField] private TMP_Text descriptionText;
        [SerializeField] private Image formulaImage;

        [SerializeField] private Button showInfoButton;
        
        public void Configurate(string text, Sprite formulaSprite)
        {
            descriptionText.text = text;
            formulaImage.sprite = formulaSprite;
        }

        public void SetActiveButton(bool isActive)
        {
            showInfoButton.gameObject.SetActive(isActive);
        }
    }
}