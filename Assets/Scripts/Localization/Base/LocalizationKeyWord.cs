﻿using System;
using UnityEngine;

namespace Localization
{
    [Serializable]
    public class LocalizationKeyWord
    {
        public string key;
        [TextArea(1, 10)] public string word;
    }
}