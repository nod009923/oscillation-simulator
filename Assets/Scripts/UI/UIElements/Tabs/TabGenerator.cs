﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Info;
using Info.Data;
using Localization;
using Oscillations.Base;
using Oscillations.Base.Functions;
using UnityEngine;

namespace UI.UIElements
{
    public class TabGenerator : MonoBehaviour
    {
        [SerializeField] private TabGroup tabGroup;
        [SerializeField] private Tab tabPrefab;
        [SerializeField] private Transform container;
        
        
        private void Start()
        {
            Generate();
        }

        private void Generate()
        {
            List<Tab> generatedTabs = new List<Tab>();
            
            var asm = Assembly.GetAssembly(typeof(Tab));

            foreach (var type in asm.GetTypes().Reverse())
            {
                if (type.IsClass && type.GetInterface("ICalculator") != null)
                {
                    generatedTabs.Add(Instantiate(tabPrefab, container));
                    generatedTabs[generatedTabs.Count - 1].Header = LocalizationManager.Instantiate.Translate($"[{type.Name}]");

                    generatedTabs[generatedTabs.Count - 1].OnSelect.AddListener(delegate(Tab tab)
                    {
                        GameObject obj = OscillationsManager.Instance.GetCalculator(type);
                        obj.SetActive(true);
                            
                        var data = obj.GetComponent<InfoDataComponent>().data;
                        InfoWindow.Instance.Configurate(LocalizationManager.Instantiate.Translate(data.KeyText), data.FormulaSprite);
                        InfoWindow.Instance.SetActiveButton(true);
                    });
                    
                    generatedTabs[generatedTabs.Count - 1].OnDeselect.AddListener(delegate(Tab tab)
                    {
                        GameObject obj = OscillationsManager.Instance.GetCalculator(type);
                        obj.SetActive(false);
                    });
                }
            }
            
            foreach (var type in asm.GetTypes().Reverse())
            {
                if (type.IsClass && type.GetInterface("IFunction") != null)
                {
                    generatedTabs.Add(Instantiate(tabPrefab, container));
                    generatedTabs[generatedTabs.Count - 1].Header = LocalizationManager.Instantiate.Translate($"[{type.Name}]");

                    generatedTabs[generatedTabs.Count - 1].OnSelect.AddListener(delegate(Tab tab)
                    {
                        GameObject obj = FunctionsManager.Instance.GetFunction(type);
                        obj.SetActive(true);
                        
                        InfoWindow.Instance.SetActiveButton(false);
                    });
                    
                    generatedTabs[generatedTabs.Count - 1].OnDeselect.AddListener(delegate(Tab tab)
                    {
                        FunctionsManager.Instance.GetFunction(type).SetActive(false);
                    });
                }
            }
            
            tabGroup.Configurate(generatedTabs[0], generatedTabs);
        }
    }
}