﻿namespace Oscillations.Base.Data
{
    /// <summary>
    /// Абстрактный класс данных для колебаний.
    /// </summary>
    public abstract class OscillationData
    {
        /// <summary>
        /// Максимальное отклонение колеблющейся величины от положения равновесия.
        /// </summary>
        public float Amplitude;
        
        /// <summary>
        /// Время полного колебания, через который повторяются какие-либо показатели
        /// состояния системы (система совершает одно полное колебание).
        /// </summary>
        public float Period => 1 / Frequency;
        
        /// <summary>
        /// Число колебаний в единицу времени.
        /// </summary>
        public float Frequency;

        /// <summary>
        /// Фаза колебания.
        /// </summary>
        public float Phase;

        /// <summary>
        /// Частота, рассчитываемая через формулу.
        /// </summary>
        public abstract float W { get; }

        /// <summary>
        /// Тип колебания.
        /// </summary>
        public OscillationType Type;
    }
}