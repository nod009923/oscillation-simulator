﻿using UnityEngine;

namespace Oscillations.Base.Data
{
    [System.Serializable]
    public class OscillationGrowingData : OscillationData
    {
        /// <summary>
        /// Круговая частота
        /// </summary>
        public override float W => (2 * Mathf.PI) / Period;

        /// <summary>
        /// Коэффициент нарастания.
        /// </summary>
        public float GrowingValue = 2;
    }
}