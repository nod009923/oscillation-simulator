﻿using System;
using System.Collections.Generic;
using Oscillations.Base.Data;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using TimeSystem;

namespace Oscillations.Base
{
    public class GraphDrawer : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private LineRenderer lineRenderer;
        [SerializeField] private GameObject calculatorObject;

        [Header("Values")] 
        public int dotsCount = 5;

        private ICalculator _calculator;
        private IFunction _function;

        private Vector3[] GetDotsByTime(float time, float period)
        {
            List<Vector3> dots = new List<Vector3>();
            float timeStep = period / dotsCount;
            float currentTimeShift = -10;

            while (currentTimeShift < 10)
            {
                float calculateValue = _calculator == null ? _function.Calculate(currentTimeShift + time) : 
                    _calculator.Calculate(currentTimeShift + time);
                
                dots.Add(new Vector3(currentTimeShift, calculateValue, 0));
                currentTimeShift += timeStep;
            }
            
            return dots.ToArray();
        }
        
        private void Awake()
        {
            _calculator = calculatorObject.GetComponent<ICalculator>();
            _function = calculatorObject.GetComponent<IFunction>();
            
            dotsCount = dotsCount % 2 == 0 ? dotsCount++ : dotsCount;
            lineRenderer.positionCount = dotsCount;
        }

        private void Update()
        {
            dotsCount = dotsCount >= 0 ? dotsCount : 0;

            float period = _calculator == null ? _function.Period() : _calculator.GetData().Period;
            
            var dots = GetDotsByTime(TimeManager.Time, period);

            lineRenderer.positionCount = dots.Length;
            lineRenderer.SetPositions(dots);
        }
    }
}