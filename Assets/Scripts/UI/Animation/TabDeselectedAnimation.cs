﻿using System.Collections;
using UnityEngine;

namespace UI.Animation
{
    public class TabDeselectedAnimation : MonoBehaviour
    {
        [SerializeField] private float animationTime;
        [SerializeField] private float width = 300;
        
        [SerializeField] private RectTransform colorizedBarRectTransform;

        
        [ContextMenu("StartAnimation")]
        public void StartAnimation()
        {
            StartCoroutine(AnimationProcess());
        }
        
        private IEnumerator AnimationProcess()
        {
            float time = 0;

            Vector2 size = colorizedBarRectTransform.sizeDelta;

            size.x = 0;
            
            while (time < animationTime)
            {
                time += Time.deltaTime;
                yield return null;

                size.x = Mathf.Lerp(width, 0, time / animationTime);
                
                colorizedBarRectTransform.sizeDelta = size;
            }
        }
    }
}