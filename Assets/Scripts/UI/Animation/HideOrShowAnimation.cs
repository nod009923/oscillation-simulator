﻿using System.Collections;
using UnityEngine;

namespace UI.Animation
{
    public class HideOrShowAnimation : MonoBehaviour
    {
        [SerializeField] private RectTransform targetObject;
        [SerializeField] private float duration;
        
        [Space]
        [SerializeField] private Vector2 hiddenPosition;
        [SerializeField] private Vector2 showedPosition;
        
        [Space]
        [SerializeField] private AnimationCurve curve;
        
        private Coroutine currentAnimationProgress;

        private bool _isShowing;

        private float currentTimer;

        public void SetAnimationState(bool isShowing)
        {
            _isShowing = isShowing;

            currentTimer = 0;
            
            if (currentAnimationProgress != null)
                StopCoroutine(currentAnimationProgress);

            StartCoroutine(AnimationProcess());
        }

        public void StartReverseAnimation()
        {
            _isShowing = !_isShowing;
            
            currentTimer = 0;

            if (currentAnimationProgress != null)
                StopCoroutine(currentAnimationProgress);

            StartCoroutine(AnimationProcess());
        }
        
        private IEnumerator AnimationProcess()
        {
            do
            {
                if (_isShowing)
                {
                    targetObject.anchoredPosition = Vector2.Lerp(hiddenPosition, showedPosition, 
                        curve.Evaluate(currentTimer / duration));
                }
                else
                {
                    targetObject.anchoredPosition = Vector2.Lerp(showedPosition, hiddenPosition, 
                        curve.Evaluate(currentTimer / duration));
                }
                
                currentTimer += Time.deltaTime;
                yield return null;
                
            } while (currentTimer / duration < 1);

            targetObject.anchoredPosition = _isShowing ? showedPosition : hiddenPosition;
        }
    }
}