﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.OscillationParams
{
    [RequireComponent(typeof(CanvasGroup)), RequireComponent(typeof(LayoutElement))]
    public abstract class BaseParamInputElement : MonoBehaviour
    {
        [SerializeField] protected bool useClamp;
        [SerializeField] protected float minValue;
        [SerializeField] protected float maxValue;
        
        [Space]
        [SerializeField] protected CanvasGroup canvasGroup;
        [SerializeField] protected LayoutElement layoutElement;
        
        public event Action<float> OnValueChanged;

        protected float cacheValue;

        protected virtual void Awake(){}

        protected void OnInputUpdate(string text)
        {
            cacheValue = text.ParseFloat();

            if (useClamp)
            {
                cacheValue = Mathf.Clamp(cacheValue, minValue, maxValue);
            }
            
            
            OnValueChanged?.Invoke(cacheValue);
        }

        protected void OnInputUpdate(float value)
        {
            cacheValue = useClamp ? Mathf.Clamp(cacheValue, minValue, maxValue) : value;
        }

        protected virtual void OnEndEdit(string value)
        {
            cacheValue = value.ParseFloat();

            if (useClamp)
            {
                if (cacheValue < minValue)
                    SetShowingValue(minValue);
                else if (cacheValue > maxValue)
                    SetShowingValue(maxValue);
                
                cacheValue = Mathf.Clamp(cacheValue, minValue, maxValue);
            }
        }
                
        public virtual void SetActive(bool isActive)
        {
            canvasGroup.SetActive(isActive);
            layoutElement.ignoreLayout = !isActive;
        }

        public virtual void SetShowingValue(float value)
        {
        }

        protected virtual void OnDestroy()
        {
        }

        public void Reset()
        {
            OnValueChanged = null;
        }
    }
}