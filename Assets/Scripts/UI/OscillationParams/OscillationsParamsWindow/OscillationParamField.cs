﻿using UnityEngine;
using UnityEngine.Serialization;

namespace UI.OscillationParams
{
    [System.Serializable]
    public class OscillationParamField
    {
        [SerializeField] private OscillationFieldType fieldType;
        [FormerlySerializedAs("inputView")] [FormerlySerializedAs("fieldView")] [SerializeField] private BaseParamInputElement inputElement;

        public OscillationFieldType FieldType => fieldType;
        public BaseParamInputElement InputElement => inputElement;
    }
}