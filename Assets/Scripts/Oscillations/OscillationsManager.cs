﻿using System;
using System.Collections.Generic;
using Oscillations.Base.Data;
using UnityEngine;

namespace Oscillations.Base
{
    public class OscillationsManager : MonoBehaviour
    {
        public static OscillationsManager Instance;
        public OscillationsManager()
        {
            if (Instance == null)
                Instance = this;
        }
        
        [SerializeField] private GameObject[] calculatorPrefabs;
        [SerializeField] private Transform container;
        
        private readonly Dictionary<Type, GameObject> _oscillations = new Dictionary<Type, GameObject>();
        
        public GameObject GetCalculator(Type type) 
        {
            if (!_oscillations.ContainsKey(type))
            {
                _oscillations.Add(type, CreateNewCalculator(type));
            }

            return _oscillations[type];
        }

        private GameObject CreateNewCalculator(Type type)
        {
            foreach (var calculatorPrefab in calculatorPrefabs)
            {
                ICalculator calculator = calculatorPrefab.GetComponent<ICalculator>();
                if (calculator.GetType() == type)
                {
                    return Instantiate(calculatorPrefab, container);
                }
            }

            return null;
        }
    }
}