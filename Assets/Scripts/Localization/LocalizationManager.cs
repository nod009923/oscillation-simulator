﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Localization
{
    public class LocalizationManager : MonoBehaviour
    {
        public static LocalizationManager Instantiate;
        
        private readonly string path = "Configs/Localization";
        [SerializeField] private SystemLanguage targetLanguage;
        [Space]
        [SerializeField] private List<LocalizationLanguageVocabularyPair> _localizationVocabularies;

        
        private LocalizationVocabulary _currentVocabulary;

        
        private void Awake()
        {
            if (Instantiate == null)
                Instantiate = this;

#if UNITY_EDITOR
            _currentVocabulary = GetVocabularyByLanguage(targetLanguage);
#else
            _currentVocabulary = GetVocabularyByLanguage(Application.systemLanguage);
#endif
        }

        private LocalizationVocabulary GetVocabularyByLanguage(SystemLanguage language)
        {
            foreach (var pair in _localizationVocabularies)
            {
                if (pair.Language == language)
                {
                    return pair.Vocabulary;
                }
            }

            return _localizationVocabularies[0].Vocabulary;
        }

        public string Translate(string key)
        {
            return _currentVocabulary.Translate(key);
        }
    }
}