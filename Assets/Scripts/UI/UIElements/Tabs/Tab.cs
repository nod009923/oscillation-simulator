﻿using System;
using TMPro;
using UI.Animation;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UI.UIElements
{
    public class Tab : MonoBehaviour, IPointerClickHandler
    {
        public bool IsSelected { get; private set; } = false;

        [SerializeField] public OnSelectedEvent<Tab> OnSelect;
        [SerializeField] public OnDeselectedEvent<Tab> OnDeselect;
        [SerializeField] private TMP_Text label;

        public string Header
        {
            get => label.text;
            set => label.text = value;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            OnSelected();
        }
        
        public void Select()
        {
            OnSelected();
        }
        
        public void Deselect()
        {
            OnDeselected();
        }

        private void OnSelected()
        {
            OnSelect?.Invoke(this);

            IsSelected = true;
        }

        private void OnDeselected()
        {
            OnDeselect?.Invoke(this);
            
            IsSelected = false;
        }
    }
    
    [Serializable] public class OnSelectedEvent<T> : UnityEvent<T> where T : MonoBehaviour {}
    [Serializable] public class OnDeselectedEvent<T> : UnityEvent<T> where T : MonoBehaviour {}
}