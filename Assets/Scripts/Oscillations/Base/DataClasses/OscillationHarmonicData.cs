﻿using UnityEngine;

namespace Oscillations.Base.Data
{
    /// <summary>
    /// Данные для гармонических колебаний.
    /// </summary>
    [System.Serializable]
    public class OscillationHarmonicData : OscillationData
    {
        /// <summary>
        /// Круговая частота.
        /// </summary>
        public override float W => (2 * Mathf.PI) / Period;
    }
}