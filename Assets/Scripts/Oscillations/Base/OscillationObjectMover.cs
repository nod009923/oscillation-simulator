﻿using System;
using Oscillations.Base.Data;
using TimeSystem;
using UnityEngine;
using UnityEngine.UI;

namespace Oscillations.Base
{
    /// <summary>
    /// Компонент для движения объекта по заданному колебанию.
    /// </summary>
    public class OscillationObjectMover : MonoBehaviour
    {
        [SerializeField] private GameObject calculatorObject;
        
        private ICalculator _calculator;

        private IFunction _function;
        
        private void Awake()
        {
            _calculator = calculatorObject.GetComponent<ICalculator>();

            _function = calculatorObject.GetComponent<IFunction>();
        }
        
        private void Update()
        {
            float y = _calculator == null ? _function.Calculate(TimeManager.Time) :
                _calculator.Calculate(TimeManager.Time);

            transform.position = new Vector3(0, y, 0);
        }
    }
}