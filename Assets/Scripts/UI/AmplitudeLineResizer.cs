﻿using System;
using Oscillations.Base;
using Oscillations.Base.Data;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace UI
{
    public class AmplitudeLineResizer : MonoBehaviour
    {
        [Header("References")]
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private GameObject calculatorObject;

        [Header("Value")]
        [SerializeField] private float amplitudeOneSizeValue = 215;
        
        private ICalculator _calculator;

        private void Awake()
        {
            _calculator = calculatorObject.GetComponent<ICalculator>();
        }
        
        private void Update()
        {
            rectTransform.sizeDelta =
                new Vector2(rectTransform.sizeDelta.x, amplitudeOneSizeValue * _calculator.GetData().Amplitude);
        }
    }
}