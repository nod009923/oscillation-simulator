﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace UI.UIContollers
{
    [System.Serializable]
    public class RecolorUIGroup
    {
        public string groupName;
        public Color color;
        public List<Image> targetImages;
    }
}