﻿using UnityEngine;
using Oscillations.Base.Data;

namespace Oscillations.Base
{
    public interface ICalculator
    {
        OscillationData GetData();
        float Calculate(float time);
    }
}