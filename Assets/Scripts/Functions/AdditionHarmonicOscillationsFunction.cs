﻿using System.Collections.Generic;
using Oscillations.Base.Calculators;
using Oscillations.Base.Data;
using TimeSystem;
using UI.OscillationParams;
using UnityEngine;

namespace Oscillations.Base.Functions
{
    public class AdditionHarmonicOscillationsFunction : MonoBehaviour, IFunction
    {
        [SerializeField] private SumHarmonicOscillationData[] calculators;

        public float Period() => calculators[0].Period > calculators[1].Period ? calculators[0].Period : calculators[1].Period;
        
        private OscillationsParamsView[] _paramsView;
        
        public float Calculate(float time)
        {
            var xData = calculators[0];
            var yData = calculators[1];

            float A = (xData.Amplitude * xData.Amplitude) + (yData.Amplitude * yData.Amplitude) +
                      2 * xData.Amplitude * yData.Amplitude *
                      Mathf.Cos(yData.W * time - xData.W * time);

            return A * Mathf.Cos(xData.W * time);
        }
        
        private void OnEnable()
        {
            OscillationType[] types = new OscillationType[calculators.Length];
            
            _paramsView = OscillationParamsWindow.Instance.ShowParams(types);

            for (var i = 0; i < _paramsView.Length; i++)
            {
                foreach (var field in _paramsView[i].Fields)
                {
                    Subscribe(calculators[i], field.FieldType, field.InputElement);
                }
            }
        }
        
        private void Subscribe(SumHarmonicOscillationData data, OscillationFieldType type, BaseParamInputElement element)
        {
            switch (type)
            {
                case OscillationFieldType.Amplitude:
                    element.OnValueChanged += delegate(float value) { AmplitudeUpdate(data, value); };
                    element.SetShowingValue(data.Amplitude);
                    break;
                
                case OscillationFieldType.Frequency:
                    element.OnValueChanged += delegate(float value) { FrequencyUpdate(data, value); };;
                    element.SetShowingValue(data.Frequency);
                    break;
                
                case OscillationFieldType.Phase:
                    element.OnValueChanged += delegate(float value) { PhaseUpdate(data, value); };;
                    element.SetShowingValue(data.Phase);
                    break;
            }
        }

        #region InputFields UpdateEventMethods
        private void AmplitudeUpdate(SumHarmonicOscillationData data, float value)
        {
            data.Amplitude = value;
        }
        
        private void FrequencyUpdate(SumHarmonicOscillationData data, float value)
        {
            data.Frequency = value;
        }
        
        private void PhaseUpdate(SumHarmonicOscillationData data, float value)
        {
            data.Phase = value;
        }
        #endregion
    }
}