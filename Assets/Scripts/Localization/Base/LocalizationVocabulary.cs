﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Localization
{
    [CreateAssetMenu(fileName = "Localization", menuName = "Localization/New Vocabulary")]
    public class LocalizationVocabulary : ScriptableObject
    {
        [SerializeField] private List<LocalizationKeyWord> vocabulary;

        public string Translate(string key)
        {
            foreach (var keyWord in vocabulary)
            {
                if (keyWord.key == key)
                    return keyWord.word;
            }

            return "NOT_LOCALIZATION";
        }
    }
}