﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UI.UIContollers;
using UnityEngine;
using UnityEngine.UI;

public class UIColorConfigurator : MonoBehaviour
{
    [SerializeField] private List<RecolorUIGroup> recolorObjects = new List<RecolorUIGroup>();

    private void OnValidate()
    {
        Recolor();
    }

    private void Recolor()
    {
        foreach (var uiGroup in recolorObjects)
        {
            if (uiGroup.targetImages != null)
            {
                foreach (var targetImage in uiGroup.targetImages)
                {
                    if(targetImage != null)
                        targetImage.color = uiGroup.color;
                }
            }
        }
    }
}
