﻿using System;
using Localization;
using Oscillations.Base.Data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.OscillationParams
{
    public class OscillationsParamsView : MonoBehaviour
    {
        [SerializeField] private TMP_Text oscillationName;
        
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private LayoutElement layoutElement;
        
        [Space]
        [SerializeField] private OscillationParamField[] fields;

        public bool IsActive => canvasGroup.interactable;
        public OscillationParamField[] Fields => fields;
        
        
        public void Configurate(OscillationType oscillationType, OscillationFieldType[] types)
        {
            oscillationName.text = LocalizationManager.Instantiate.Translate($"[{oscillationType.ToString()}]");
            
            foreach (var field in fields)
            {
                field.InputElement.Reset();

                bool flag = false;
                foreach (var type in types)
                {
                    if (!flag)
                        field.InputElement.SetActive(flag = field.FieldType == type);
                }
            }
        }

        public void SetActive(bool isActive)
        {
            canvasGroup.SetActive(isActive);
            layoutElement.ignoreLayout = !isActive;
        }
    }
}