﻿using System;
using UnityEngine;

namespace Oscillations.Base.Data
{
    [Serializable]
    public class SumHarmonicOscillationData : OscillationData
    {
        public override float W => (4 * Mathf.PI) / Period;
    }
}