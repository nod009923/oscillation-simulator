﻿using TMPro;
using UnityEngine;

namespace UI.OscillationParams
{
    public class ParamInputFieldElement : BaseParamInputElement
    {
        [SerializeField] private TMP_InputField inputField;

        protected override void Awake()
        {
            base.Awake();
            inputField.onValueChanged.AddListener(OnInputUpdate);
            inputField.onEndEdit.AddListener(OnEndEdit);
        }

        public override void SetShowingValue(float value)
        {
            base.SetShowingValue(value);
            inputField.text = $"{value}";
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            inputField.onValueChanged.RemoveListener(OnInputUpdate);
            inputField.onEndEdit.RemoveListener(OnEndEdit);
        }
    }
}