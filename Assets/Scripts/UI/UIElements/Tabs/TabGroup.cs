﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace UI.UIElements
{
    public class TabGroup : MonoBehaviour
    {
        [SerializeField] private Tab selected;
        [SerializeField] private List<Tab> tabs;

        public void Configurate(Tab selectedTab, List<Tab> tabsList)
        {
            selected = selectedTab;
            tabs = tabsList;
            
            if (selected == null && tabs.Count > 0)
                selected = tabs[0];

            foreach (var tab in tabs)
            {
                tab.OnSelect.AddListener(OnTabSelected);
            }
            
            selected.Select();
        }

        private void OnTabSelected(Tab target)
        {
            if(selected == target) 
                return;
            
            selected.Deselect();

            selected = target;
        }
    }
}