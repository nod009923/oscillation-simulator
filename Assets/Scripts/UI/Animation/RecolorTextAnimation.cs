﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace UI.Animation
{
    public class RecolorTextAnimation : MonoBehaviour
    {
        [SerializeField] private Gradient colorGradient;
        [SerializeField] private float animationTime;
        [SerializeField] private TMP_Text targetText;

        [ContextMenu("StartAnimation")]
        public void StartAnimation()
        {
            StopAllCoroutines();
            StartCoroutine(AnimationProcess());
        }

        [ContextMenu("StartReverseAnimation")]
        public void StartReverseAnimation()
        {
            StopAllCoroutines();
            StartCoroutine(ReverseAnimationProcess());
        }
        
        private IEnumerator AnimationProcess()
        {
            float time = 0;

            while (time < animationTime)
            {
                time += Time.deltaTime;
                yield return null;
                targetText.color = colorGradient.Evaluate(time / animationTime);
            }
        }
        
        private IEnumerator ReverseAnimationProcess()
        {
            float time = 0;

            while (time < animationTime)
            {
                time += Time.deltaTime;
                yield return null;
                targetText.color = colorGradient.Evaluate(1 - (time / animationTime));
            }
        }
    }
}