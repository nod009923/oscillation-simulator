﻿using UnityEngine;
using UnityEngine.Serialization;

namespace Oscillations.Base.Data
{
    [System.Serializable]
    public class OscillationDampingData : OscillationData
    {
        /// <summary>
        /// Круговая частота
        /// </summary>
        public override float W => (2 * Mathf.PI) / Period;

        /// <summary>
        /// Коэффициент затухания.
        /// </summary>
        public float DampingValue = 2;
    }
}