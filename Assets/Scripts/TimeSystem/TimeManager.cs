﻿using System;
using UI.OscillationParams;
using UnityEngine;
using UnityEngine.UI;

namespace TimeSystem
{
    public class TimeManager : MonoBehaviour
    {
        [SerializeField] private Slider timeScaleSlider;
        
        [SerializeField] private float timeScale = 1;
        
        private static float time;
        public static float Time => time;

        private void Awake()
        {
            timeScaleSlider.onValueChanged.AddListener(OnFieldValueChanged);
        }

        private void OnFieldValueChanged(float value)
        {
            timeScale = value;
        }

        private void Update()
        {
            time += UnityEngine.Time.deltaTime * timeScale;
        }

        public static void ResetTime()
        {
            time = 0;
        }

        private void OnDestroy()
        {
            timeScaleSlider.onValueChanged.AddListener( OnFieldValueChanged);
        }
    }
}