﻿namespace Oscillations.Base.Data
{

    /// <summary>
    /// Типы колебаний.
    /// </summary>
    public enum OscillationType
    {
        Harmonic,
        Damped,
        Grow
    }
}