﻿using System;
using Oscillations.Base.Data;
using TMPro;
using UI.OscillationParams;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;
using Utils;

namespace Oscillations.Base.Calculators
{
    public class DampingOscillationCalculator : MonoBehaviour, ICalculator
    {
        [Header("Values")]
        [SerializeField] private OscillationDampingData data;

        private OscillationsParamsView _paramsView;

        public OscillationData GetData() => data;
        
        public float Calculate(float time)
        {
            return data.Amplitude * (Mathf.Pow(2.718f, - data.DampingValue * time))
                                   * Mathf.Sin(data.W * time + data.Phase);
        }
        
        private void OnEnable()
        {
            _paramsView = OscillationParamsWindow.Instance.ShowParams(data.Type);
            
            foreach (var field in _paramsView.Fields)
            {
                Subscribe(field.FieldType, field.InputElement);
            }
        }

        private void Subscribe(OscillationFieldType type, BaseParamInputElement element)
        {
            switch (type)
            {
                case OscillationFieldType.Amplitude:
                    element.OnValueChanged += AmplitudeUpdate;
                    element.SetShowingValue(data.Amplitude);
                    break;
                
                case OscillationFieldType.Frequency:
                    element.OnValueChanged += FrequencyUpdate;
                    element.SetShowingValue(data.Frequency);
                    break;
                
                case OscillationFieldType.Phase:
                    element.OnValueChanged += PhaseUpdate;
                    element.SetShowingValue(data.Phase);
                    break;

                case OscillationFieldType.Damping:
                    element.OnValueChanged += DampingUpdate;
                    element.SetShowingValue(data.DampingValue);
                    break;
            }
        }
        
        private void Unsubscribe(OscillationFieldType type, BaseParamInputElement element)
        {
            switch (type)
            {
                case OscillationFieldType.Amplitude:
                    element.OnValueChanged -= AmplitudeUpdate;
                    break;
                
                case OscillationFieldType.Frequency:
                    element.OnValueChanged -= FrequencyUpdate;
                    break;
                
                case OscillationFieldType.Phase:
                    element.OnValueChanged -= PhaseUpdate;
                    break;

                case OscillationFieldType.Damping:
                    element.OnValueChanged -= DampingUpdate;
                    break;
            }
        }
        
        #region InputFields UpdateEventMethods
        private void AmplitudeUpdate(float value)
        {
            data.Amplitude = value;
        }
        
        private void FrequencyUpdate(float value)
        {
            data.Frequency = value;
        }
        
        private void PhaseUpdate(float value)
        {
            data.Phase = value;
        }
        
        private void DampingUpdate(float value)
        {
            data.DampingValue = value;
        }
        #endregion
        
        private void OnDisable()
        {
            foreach (var field in _paramsView.Fields)
            {
                Unsubscribe(field.FieldType, field.InputElement);
            }
        }
    }
}