﻿namespace UI.OscillationParams
{
    public enum OscillationFieldType
    {
        Amplitude,
        Frequency,
        Phase,
        Growing,
        Damping
    }
}